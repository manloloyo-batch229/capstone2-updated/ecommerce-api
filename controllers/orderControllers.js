const Orders = require('../models/Orders.js');
const Products = require('../models/Products.js');

module.exports.createOrder = async (data) => {
    const { user, order } = data;

    // Will check if the user is logged in.
    if (!user) {
        return "You are not logged in yet, Please Login First";
    }

    const product = await Products.findById(order._id);
    if (product) {
        let totalAmount = product.price * order.quantity;
        let newOrder = new Orders({
            userId : user.id,
            products : {
                productId : product._id,
                quantity : order.quantity
            },
            totalAmount,
            purchasedOnDate : new Date()
        })

        await newOrder.save();
        console.log(product);
        return "Purchased Successfully";

    } else {
        
        return "Product not found!"
    }
}

// Will check all the Orders but Admin Only
module.exports.getAllOrders = (data) => {

    if(data.isAdmin) {
        return Orders.find({}).then((result, err) => {
            if(err){
                return false;
            }else {
                console.log(result);
                return result;
            }
        });
    }
};