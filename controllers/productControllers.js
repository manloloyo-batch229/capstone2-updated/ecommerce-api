const Products = require("../models/Products.js");

// Create a product - ADMIN
module.exports.addNewProduct = async (user, reqBody) => {
    const {name, description, price, author} = reqBody;
    if (user) {
        let newProduct = new Products({ name, description, price});
        await newProduct.save();
        //return true
        return "The new product has been successfully created";
    } else {
        // return false
        return "You are not an admin!";
    }
};


// Get all active products
module.exports.getActiveProducts = async () => {
    const result = await Products.find({ isActive: true });
    return result.length > 0 ? result : { message: "No active products" };
};

// Get a single product
module.exports.specificProduct = (reqParams) => {
    return Products.findById(reqParams.productId).then(result => {
        return result;
    })
}


// Update Product for admin only 
    module.exports.updateProduct = async (reqParams, reqBody) => {
    console.log(reqParams);
    console.log(reqBody);

    let updateProduct = {
        name : reqBody.name,
        description : reqBody.description,
        price : reqBody.price
    };

    return await Products.findByIdAndUpdate(reqParams.productId, updateProduct).then((product, err) => {
        if(err){
            return false;
        }else{

            return `The Item has been Updated.`;
        }
    });
    
};
    
// Archive a product - ADMIN
module.exports.archiveProduct = async (data) => {

if(data.isAdmin === true) {
    let hideActiveField = {
        isActive : false
    };

    return await Products.findByIdAndUpdate(data.reqParams.productId, hideActiveField).then((product, err) => {
        if(err){

            return false

        }else{

            return true
        }
    })
    }else {
    return false
    }
};
