const Users = require('../models/Users.js');
const bcrypt = require('bcrypt');
const auth = require('../auth.js');



// Register New User.
module.exports.registerNewUser = async (reqBody) => {
    const userExisting = await Users.findOne({email : reqBody.email});
    if (userExisting) {
        return "User already exists, Please try another email!" ;
    }
    let newUser = new Users({
        email : reqBody.email,
        password : bcrypt.hashSync(reqBody.password, 10),
        isAdmin : reqBody.isAdmin
    })
    await newUser.save();
    
    console.log(newUser);

    return  "User created successfully";
}

// User Authentication
module.exports.logInUser = (reqBody) => {
    return Users.findOne({email: reqBody.email})
        .then((result) => {
            if (result == null) {
                return "The Entered email does not exists";
            } else {
                const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

                return (isPasswordCorrect) ? {access : auth.createAccessTokens(result)} : false;
            }
        }
    )
}

// Get All User
module.exports.getAllProfile = async user => {
    if (!user.isAdmin) {

        return "You are not an admin!";
    }

    const users = await Users.find({}, '_id -password');

    return users;
};

// Retrieve User Details
module.exports.getUserDetails = (data) => {
    console.log(data)
    return Users.findById(data.userId).then(result => {
        console.log(result);

        result.password = "";

        return result;
    });
}


