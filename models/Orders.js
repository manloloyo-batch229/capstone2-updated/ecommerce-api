const mongoose = require('mongoose');

const ordersSchema = new mongoose.Schema({
    userId : {
        type : mongoose.Schema.Types.ObjectId,
        required : [true, 'User ID is required']
    }, 
    products : [{
        productId : {
            type : String,
            required : [true, 'Product ID is required']
        },
        quantity : {
            type : Number,
            required : [true, 'Quantity is required']
        }
    }],
    totalAmount : {
        type : Number,
        required : [true, 'Total Amount is required']
    },
    purchasedOnDate : {
        type : Date,
        default : Date.now
    }
});

module.exports = mongoose.model('Orders', ordersSchema);