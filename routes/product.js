const express = require("express");
const router = express.Router();
const productControllers = require("../controllers/productControllers.js");
const auth = require("../auth.js");

// Create a product
router.post("/createproduct", auth.verify, (req, res) => {
    const data = auth.decode(req.headers.authorization).isAdmin;
    
    productControllers.addNewProduct(data, req.body).then((resultFromController) => res.send(resultFromController));

});

// Get all active products
router.get("/allactive", (req, res) => {

    productControllers.getActiveProducts().then((resultFromController) => res.send(resultFromController));
});

// Retrieve Single Product
router.get("/:productId", (req,res) => {
    productControllers.specificProduct(req.params).then((resultFromController) => res.send(resultFromController));

    });

// Update Product for admin only
 router.put("/:productId", auth.verify, (req,res) => {

    productControllers.updateProduct(req.params, req.body).then(resultFromController => res.send(resultFromController));
})

/*outer.put('/update', auth.verify, (req, res) => {
    const user = auth.decode(req.headers.authorization);
    const productID = req.body._id;
    productControllers
        .updateProduct(user, productID, req.body)
        .then((resultFromController) => res.send(resultFromController));
});
*/
// Archive a product (Admin Only)
router.put("/archive/:productId", auth.verify, (req, res) => {
    const data = {
        reqParams: req.params,
        isAdmin: auth.decode(req.headers.authorization).isAdmin

    }
    productControllers.archiveProduct(data).then(resultFromController => res.send(resultFromController));
});

module.exports = router;
