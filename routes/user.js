const express = require('express');
const router = express.Router();
const userControllers = require('../controllers/userControllers.js');
const auth = require('../auth.js');



// Register New User Route
router.post("/register", (req, res) => {
    userControllers.registerNewUser(req.body).then(resultFromController => res.send(resultFromController));

    }
);

// User Authentication Route
router.post("/loginuser", (req, res) => {
    userControllers.logInUser(req.body).then(resultFromController => res.send(resultFromController)

    );
});

// Get All User (For Admin only)
router.get('/alluser/', auth.verify, (req, res) => {
    const user = auth.decode(req.headers.authorization);
    userControllers.getAllProfile(user).then(resultFromController => res.send(resultFromController));
    }
);

// Retrieve User Details

// Retrieving user details 
router.get("/userdetails", auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization)

    userControllers.getUserDetails({userId: req.body.id}).then(resultFromController => res.send(resultFromController));
})




module.exports = router;