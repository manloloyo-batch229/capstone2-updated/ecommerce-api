const express = require('express');
const router = express.Router();
const orderControllers = require('../controllers/orderControllers.js');
const auth = require('../auth.js');

router.post("/checkout", auth.verify, (req, res) => {
    const data = {
        user : auth.decode(req.headers.authorization),
        order : req.body,
    };

    orderControllers.createOrder(data).then((resultFromController) => res.send(resultFromController));
})

// Stretch Goal

// Retrieve All Orders routes (Admin only)

router.get("/allorders", auth.verify, (req,res) => {
    const userData = auth.decode(req.headers.authorization);

    orderControllers.getAllOrders(userData).then((resultFromController) => {
        res.send(resultFromController);
    });
});
module.exports = router;