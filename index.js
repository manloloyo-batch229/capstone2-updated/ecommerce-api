const express = require('express');
const mongoose = require('mongoose');
const userRoutes = require('./routes/user.js');
const productRoutes = require('./routes/product.js');
const orderRoutes = require('./routes/order.js');

const app = express();

// Database Connection
mongoose.connect("mongodb+srv://admin:admin1234@cluster0.yy11iud.mongodb.net/capstone2-new?retryWrites=true&w=majority", {
    useNewUrlParser: true,
    useUnifiedTopology: true
});

// Notification for database connection status.
let db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error'));
db.once('open', () => console.log('We are connected to the cloud database.'));

// Middlewares
app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use('/users', userRoutes);
app.use('/products', productRoutes);
app.use('/orders', orderRoutes);

// Server Listen
app.listen(process.env.PORT || 4000, () => {
    console.log(`API is now online on port ${process.env.PORT || 4000}`);
})